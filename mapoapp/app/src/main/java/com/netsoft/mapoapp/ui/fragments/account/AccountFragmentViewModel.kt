package com.netsoft.mapoapp.ui.fragments.account

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AccountFragmentViewModel : ViewModel() {

    val latLiveData = MutableLiveData<Double>()

    val lngLiveData = MutableLiveData<Double>()

    val photoUriLiveData = MutableLiveData<Uri>()

    fun updatePhotoUri(photoUri: Uri) {
        photoUriLiveData.postValue(photoUri)
    }

    fun updateLat(lat: Double) {
        latLiveData.postValue(lat)
    }

    fun updateLng(lng: Double) {
        lngLiveData.postValue(lng)
    }

    init {
        latLiveData.postValue(50.425684)
        lngLiveData.postValue(30.515595)
    }

}