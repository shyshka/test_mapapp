package com.netsoft.mapoapp.ui.fragments.account

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.netsoft.mapoapp.R
import com.netsoft.mapoapp.models.AccountSharedModel
import com.netsoft.mapoapp.utils.BitmapUtils
import com.netsoft.mapoapp.utils.ViewUtils.hideKeyboard
import kotlinx.android.synthetic.main.fragment_account.*

class AccountFragment : Fragment() {

    private val mViewModel: AccountFragmentViewModel by activityViewModels()

    private val REQUEST_GALLERY_IMAGE: Int = 1001

    private var onAccountSettingsChangedListener: OnAccountSettingsChangedListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_account, container, false)

    @SuppressLint("CutPasteId")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupClickListeners()

        initViewModel()
    }

    private fun initViewModel() {
        mViewModel.photoUriLiveData.observe(viewLifecycleOwner, {
            iv_photo.setImageURI(it)
        })

        mViewModel.latLiveData.observe(viewLifecycleOwner, {
            et_lat.setText(it.toString())
        })

        mViewModel.lngLiveData.observe(viewLifecycleOwner, {
            et_lng.setText(it.toString())
        })

    }

    private fun setupClickListeners() {
        btn_go.setOnClickListener {
            onBtnGoClicked()
        }

        btn_set_photo.setOnClickListener {
            onBtnSetPhotoClicked()
        }
    }

    private fun onBtnSetPhotoClicked() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, REQUEST_GALLERY_IMAGE)
    }

    private fun onBtnGoClicked() {
        hideKeyboard(requireView())

        val lat: Double

        val lng: Double

        try {
            lat = et_lat.text.toString().toDouble()
        } catch (ex: Exception) {
            Toast.makeText(requireContext(), ex.message, Toast.LENGTH_LONG).show()
            return
        }
        mViewModel.updateLat(lat)

        try {
            lng = et_lng.text.toString().toDouble()
        } catch (ex: Exception) {
            Toast.makeText(requireContext(), ex.message, Toast.LENGTH_LONG).show()
            return
        }
        mViewModel.updateLng(lng)

        val accountSharedModel = AccountSharedModel(lat, lng, loadMarkerBitmap())

        onAccountSettingsChangedListener?.onAccountSettingsChanged(accountSharedModel)
    }

    private fun loadMarkerBitmap(): Bitmap? {
        var bitmapMarker: Bitmap? = null

        mViewModel.photoUriLiveData.value?.let { uri ->
            val bitmap = MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, uri)
            bitmapMarker = BitmapUtils.scaleBitmap(bitmap)
        }

        return bitmapMarker
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        handleGalleryImage(requestCode, resultCode, data)
    }

    private fun handleGalleryImage(requestCode: Int, resultCode: Int, data: Intent?) {

        if (resultCode != RESULT_OK || requestCode != REQUEST_GALLERY_IMAGE) {
            return
        }

        data?.data?.let {
            mViewModel.updatePhotoUri(it)
        }

    }

    fun setAccountSettingsChangedListener(listener: OnAccountSettingsChangedListener) {
        onAccountSettingsChangedListener = listener
    }

    interface OnAccountSettingsChangedListener {
        fun onAccountSettingsChanged(accountSharedModel: AccountSharedModel)
    }

}