package com.netsoft.mapoapp.ui.activities.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.netsoft.mapoapp.models.AccountSharedModel

class MainActivityViewModel : ViewModel() {

    val accountSettingsLiveData = MutableLiveData<AccountSharedModel>()

    fun updateAccountSettings(value: AccountSharedModel) {
        accountSettingsLiveData.postValue(value)
    }

}