package com.netsoft.mapoapp.models

import android.graphics.Bitmap

data class AccountSharedModel(
    val lat: Double,
    var lng: Double,
    val bitmap: Bitmap?
)
