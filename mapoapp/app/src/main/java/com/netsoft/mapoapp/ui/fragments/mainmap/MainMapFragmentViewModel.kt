package com.netsoft.mapoapp.ui.fragments.mainmap

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.netsoft.mapoapp.models.AccountSharedModel

class MainMapFragmentViewModel : ViewModel() {

    val mapSettingsLiveData = MutableLiveData<AccountSharedModel>()

    fun updateMapSettings(sharedModel: AccountSharedModel) {
        mapSettingsLiveData.postValue(sharedModel)
    }

}
