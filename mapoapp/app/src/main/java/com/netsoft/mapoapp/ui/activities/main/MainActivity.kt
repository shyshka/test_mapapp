package com.netsoft.mapoapp.ui.activities.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.netsoft.mapoapp.R
import com.netsoft.mapoapp.models.AccountSharedModel
import com.netsoft.mapoapp.ui.fragments.account.AccountFragment
import com.netsoft.mapoapp.ui.fragments.mainmap.MainMapFragment

class MainActivity : AppCompatActivity(), AccountFragment.OnAccountSettingsChangedListener {

    private val mViewModel: MainActivityViewModel by viewModels()

    private lateinit var mFrgMainMap: MainMapFragment

    private lateinit var mFrgAccount: AccountFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initFragments()

        initViewModel()
    }

    private fun initViewModel() {
        mViewModel.accountSettingsLiveData.observe(this, {
            mFrgMainMap.updateMapSettings(it)
        })
    }

    private fun initFragments() {
        mFrgAccount = AccountFragment()
        mFrgAccount.setAccountSettingsChangedListener(this)

        mFrgMainMap = MainMapFragment()

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.lt_frg_account, mFrgAccount)
            .replace(R.id.lt_frg_map, mFrgMainMap)
            .addToBackStack(null)
            .commit()
    }

    override fun onAccountSettingsChanged(accountSharedModel: AccountSharedModel) {
        mViewModel.updateAccountSettings(accountSharedModel)
    }

}