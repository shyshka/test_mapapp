package com.netsoft.mapoapp.ui.fragments.mainmap

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.netsoft.mapoapp.R
import com.netsoft.mapoapp.models.AccountSharedModel
import com.netsoft.mapoapp.utils.BitmapUtils

class MainMapFragment : Fragment(), OnMapReadyCallback {

    private val mViewModel: MainMapFragmentViewModel by activityViewModels()

    private var mGoogleMap: GoogleMap? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_main_map, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initGoogleMap()

        initViewModel()
    }

    private fun initViewModel() {
        mViewModel.mapSettingsLiveData.observe(viewLifecycleOwner, {
            updateMap(it)
        })
    }

    private fun initGoogleMap() {
        val googleMapFragment =
            childFragmentManager.findFragmentById(R.id.google_map) as SupportMapFragment
        googleMapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.let {
            mGoogleMap = it
            it.uiSettings.isZoomControlsEnabled = true
        }
    }

    fun updateMapSettings(mapData: AccountSharedModel) {
        mViewModel.updateMapSettings(mapData)
    }

    private fun updateMap(mapMarker: AccountSharedModel) {

        val location = LatLng(mapMarker.lat, mapMarker.lng)

        mGoogleMap?.run {
            clear()
            addMarker(buildMarkerOptions(mapMarker))
            animateCamera(CameraUpdateFactory.newLatLngZoom(location, 17f))
        }
    }

    private fun buildMarkerOptions(mapMarker: AccountSharedModel): MarkerOptions? {

        var markerOptions = MarkerOptions()
            .position(LatLng(mapMarker.lat, mapMarker.lng))

        mapMarker.bitmap?.let {

            val markerBitmap = BitmapUtils.bitmapOverlayToCenter(
                BitmapUtils.bitmapFromVector(
                    requireContext(),
                    R.drawable.ic_baseline_marker
                ), mapMarker.bitmap
            )

            markerOptions = markerOptions.icon(BitmapDescriptorFactory.fromBitmap(markerBitmap))
        }

        return markerOptions
    }

}